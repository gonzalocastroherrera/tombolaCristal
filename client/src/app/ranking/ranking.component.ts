import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource, MatSnackBar} from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit  {
  displayedColumns = ['index', 'name', 'rut', 'local', 'total'];
  displayedColumns2 = ['index', 'name', 'total'];
  dataSource;
  dataSourceTeam;

  @ViewChild(MatSort) sort: MatSort;

  constructor(private route: ActivatedRoute, private router: Router, private api: ApiServiceService, private _snackBar: MatSnackBar) { }
  async ngOnInit() {
    let jugadores = await this.api.rankingJugador();
    jugadores = jugadores.map((jugador, index) => {
      return {
        index: index +1,
        name: jugador._id.nombre,
        rut: jugador._id.rut,
        local: jugador._id.local,
        total: jugador.totalAmount
      }
    });
    const ELEMENT_DATA: Individual[] = jugadores;    
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    this.dataSource.sort = this.sort;

    let teams = await this.api.rankingTeam();
    teams = teams.map((team, index) => {
      return {
        index: index +1,
        name: team.nombre,
        total: team.total
      }
    });
    const ELEMENT_DATA2: Group[] = teams;
    this.dataSourceTeam = new MatTableDataSource(ELEMENT_DATA2);
    this.dataSourceTeam.sort = this.sort;
  }
}

export interface Individual {
  index: number;
  name: string;
  rut: string;
  local: string;
  total: string;
}

export interface Group {
  index: number;
  name: string;
  total: number;
}