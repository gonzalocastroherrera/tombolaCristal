import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formulario: FormGroup;

  constructor(private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.formulario = new FormGroup({
      'contraseña': new FormControl(null, Validators.required),
    });
  }
  submit() {
    if(this.formulario.controls['contraseña'].value === 'improntaGames10') {
      this.router.navigate(['/index']);
    } else {
      this.createSnackBar('Contraseña incorrecta');
    }
  }

  createSnackBar(message) {
    this._snackBar.open(message, null, {
      duration: 2500
    })
  }

}
