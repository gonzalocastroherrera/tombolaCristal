import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { TombolaComponent } from './tombola/tombola.component';
import { RegistraComponent } from './registra/registra.component';
import { CuponComponent } from './cupon/cupon.component';
import { IndexComponent } from './index/index.component';
import { RankingComponent } from './ranking/ranking.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'tombola', component: TombolaComponent },
    { path: 'registrar', component: RegistraComponent },
    { path: 'cupon', component: CuponComponent },
    { path: 'index', component: IndexComponent },
    { path: 'ranking', component: RankingComponent }
];

@NgModule({

  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }