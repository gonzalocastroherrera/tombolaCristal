import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-tombola',
  templateUrl: './tombola.component.html',
  styleUrls: ['./tombola.component.css'],
})
export class TombolaComponent implements OnInit {

  cupones;
  cuponSeleccionado;
  enabledButton = true;

  constructor(private route: ActivatedRoute, private router: Router, private api: ApiServiceService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  async girarTombola() {
    try {
      this.cupones = await this.api.obtenerUsuarios();
      if (this.cupones.error) {
        this.enabledButton = true;
        this._snackBar.open('No hay cupones ingresados', null, {
          duration: 2500
        });
      } else {
        this.cuponSeleccionado = this.cupones[Math.floor(Math.random() * this.cupones.length) + 0];
        this.enabledButton = false;
      }
    } catch (error) {
      this._snackBar.open('Ocurrió un error, no se realiza el concurso', null, {
        duration: 2500
      });
    }

  }

}
