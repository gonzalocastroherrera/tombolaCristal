import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cupon',
  templateUrl: './cupon.component.html',
  styleUrls: ['./cupon.component.css']
})
export class CuponComponent implements OnInit {
  formulario: FormGroup;
  options = [];
  filteredOptions: Observable<string[]>;
  constructor(private route: ActivatedRoute, private router: Router, private api: ApiServiceService, private _snackBar: MatSnackBar) { }

  async ngOnInit() {
    this.formulario = new FormGroup({
      'rut': new FormControl(null, Validators.required),
      'cantidad': new FormControl(null, [Validators.required, Validators.min(0)]),
      'puntaje': new FormControl(null, [Validators.required, Validators.min(0)])
    });

    try {
      let result = await this.api.obtenerUsuarios();
      this.options = result.map((usuario) => {
        return usuario.rut
      });
      let value = this.formulario.controls['rut'].valueChanges;
      this.filteredOptions = value
        .pipe(
          startWith(''),
          map(val => this.filter(val))
        )
    } catch (error) {
        this.createSnackBar("Ocurrio un error no se desplegarán los usuarios");
    }

  }

  filter(val: string): string[] {
    return this.options.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  async submit() {
    try{
      console.log(this.formulario.value);
      let response = await this.api.guardarCupon(this.formulario.value);
      this.createSnackBar(response.success);
      this.router.navigate(['/index']);
    } catch(error) {
      this.createSnackBar("error en el guardado de puntaje");
    }

  }

  createSnackBar(message) {
    this._snackBar.open(message, null, {
      duration: 2500
    })
  }

}
