import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiServiceService {

  //url = 'http://localhost';
  url = 'http://159.65.68.189';
  constructor(private http: HttpClient) { }

  async guardarRegistro(body) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.post<any>(`${this.url}:3001/registro`, body, { headers: headers }).toPromise();
  }

  async guardarCupon(body) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.post<any>(`${this.url}:3001/cupon`, body, { headers: headers }).toPromise();
  }

  async obtenerCupon() {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.get<any>(`${this.url}:3001/tombola`).toPromise();
  }

  async obtenerEquipos() {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.get<any>(`${this.url}:3001/equipo`).toPromise();
  }

  async obtenerUsuarios() {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.get<any>(`${this.url}:3001/usuarios`).toPromise();
  }

  async guardarEquipo(body) {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.post<any>(`${this.url}:3001/equipo`, body, { headers: headers }).toPromise();
  }

  async rankingJugador() {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.get<any>(`${this.url}:3001/rankingJugador`).toPromise();
  }

  async rankingTeam() {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    return await this.http.get<any>(`${this.url}:3001/rankingTeam`).toPromise();
  }
}
