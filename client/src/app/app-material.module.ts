import { NgModule } from '@angular/core';
import { MatSnackBarModule, MatDialogModule, MatAutocompleteModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';




@NgModule({
    exports: [MatAutocompleteModule, MatButtonModule, MatSnackBarModule,MatInputModule, MatIconModule, MatTabsModule, MatTableModule, MatDialogModule]
})

export class AppMaterialModule {}   