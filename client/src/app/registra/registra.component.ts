import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { MatSnackBar } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-registra',
  templateUrl: './registra.component.html',
  styleUrls: ['./registra.component.css']
})
export class RegistraComponent implements OnInit {
  formulario: FormGroup;
  name: string;
  optionsTeam = [];
  optionsLocal = [];

  filteredOptions: Observable<string[]>;
  filteredOptionsLocal: Observable<string[]>;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private api: ApiServiceService, private _snackBar: MatSnackBar) { }

  async ngOnInit() {
    this.formulario = new FormGroup({
      'name': new FormControl(null, Validators.required),
      'rut': new FormControl(null, Validators.required),
      'team': new FormControl(''),
      'local': new FormControl('')
    });
    Promise.all([
      this.getTeam(),
      this.getLocal()
    ]);

  }

  filter(val: string): string[] {
    return this.optionsTeam.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  filterLocal(val: string): string[] {
    return this.optionsLocal.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  async getTeam() {
    try {
      let result = await this.api.obtenerEquipos();
      this.optionsTeam = result.map((equipo) => {
        return equipo.name
      });
      let value = this.formulario.controls['team'].valueChanges;
      this.filteredOptions = value
        .pipe(
          startWith(''),
          map(val => this.filter(val))
        )
    } catch (error) {
        this.createSnackBar("Ocurrio un error no se desplegarán los equipos");
    }
  }

  async getLocal() {
    try {
      // let result = await this.api.obtenerUsuarios();
      // this.optionsLocal = result.map((usuario) => {
      //   return usuario.local
      // });
      this.optionsLocal = [
        'RUBY TUESDAY LA REINA',
        'TONY ROMAS COSTANERA',
        'TONY ROMAS P.ARAUCO',
        'EL ESTABLO',
        'BURGER TRUCK',
        'HARD ROCK CAFÉ',
        'SUBLIME'
      ];
      let value = this.formulario.controls['local'].valueChanges;
      this.filteredOptionsLocal = value
        .pipe(
          startWith(''),
          map(val => this.filterLocal(val))
        )
    } catch (error) {
        this.createSnackBar("Ocurrio un error no se desplegarán los locales");
    }
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '450px',
      data: { name: this.name }
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      if(result) {
        this.name = result;
        let guardaEquipo = await this.api.guardarEquipo({
          name: this.name
        });
        if (guardaEquipo.error) {
          this.createSnackBar(guardaEquipo.error);
        } else {
          this.name = "";
          this.getTeam();
          this.createSnackBar("Equipo ingresado correctamente");
        }
      }
    });
  }

  async submit() {
    try {
      if (this.formulario.valid) {
        this.formulario.value.puntaje = 0;
        this.formulario.value.cantidad = 0;
        let registra = await this.api.guardarRegistro(this.formulario.value);
        if (registra.error) {
          this.createSnackBar(registra.error);
        } else {
          this.createSnackBar("Usuario ingresado correctamente");
          this.router.navigate(['/index']);
        }
      } else {
        this.createSnackBar("Ingrese datos correctamente");
      }
    } catch (error) {
      this.createSnackBar("No se ha podido completar la operación");
    }
  }

  createSnackBar(message) {
    this._snackBar.open(message, null, {
      duration: 2500
    })
  }

}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-registra.html',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


