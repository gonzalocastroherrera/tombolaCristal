import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TombolaComponent } from './tombola/tombola.component';
import { RegistraComponent, DialogOverviewExampleDialog } from './registra/registra.component';
import { CuponComponent } from './cupon/cupon.component';
import { LoginComponent } from './login/login.component';
import { IndexComponent } from './index/index.component';
import { RankingComponent } from './ranking/ranking.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiServiceService } from './api-service.service';
import { AppMaterialModule } from './app-material.module';


@NgModule({
  declarations: [
    AppComponent,
    TombolaComponent,
    RegistraComponent,
    CuponComponent,
    RankingComponent,
    LoginComponent,
    IndexComponent,
    DialogOverviewExampleDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  entryComponents: [DialogOverviewExampleDialog],
  providers: [ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
