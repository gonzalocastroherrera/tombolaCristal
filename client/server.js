"use strict"
const express = require('express');
const http = require('http');
const path = require('path');
const app = express();
const port = 4200;
const bodyParser = require('body-parser');
const server = http.createServer(app);

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With");
    next();
});
app.use('/', express.static(path.join(__dirname, '/dist')))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
server.listen(port, (err) => {
    if (err) {
        throw err
    }
    console.log(`From Server: App iniciada exitosamente y corriendo en puerto: ${port}`)
})