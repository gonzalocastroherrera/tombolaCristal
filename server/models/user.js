const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    name: String,
    rut: String,
    puntaje: Number,
    cantidad: Number,
    team: String,
    local: String
});

module.exports = mongoose.model('User', userSchema);