const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const teamSchema = new Schema({
    name: String,
    players: [{
        rut: String,
        cantidad: Number,
        puntaje: Number
    }]
});

module.exports = mongoose.model('Team', teamSchema);