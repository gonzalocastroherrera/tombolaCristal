const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const cuponSchema = new Schema({
    rut: String,
    quantity: Number
});

module.exports = mongoose.model('Cupon', cuponSchema);