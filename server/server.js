const express = require('express');
const app = express();
const mongoose = require('mongoose');
const port = 3001;
const User = require('./models/user');
const Cupon = require('./models/cupon');
const Team = require('./models/team');
const bodyParser = require('body-parser');


mongoose.connect("mongodb://gnzlo:shimys123@ds251799.mlab.com:51799/negocioweb")
    .then(() => {
        console.log('connected BD');
    })
    .catch((err) => {
        console.log(`error ${err}`);
    });

app.listen(3001, () => {
    console.log(`servidor iniciado en el puerto ${port}`);
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post("/registro", async function (req, res) {
    try {
        let exists = await User.findOne({ rut: req.body.rut });
        if (exists !== null) {
            res.status(200).send({
                error: "Usuario ya existe"
            });
        } else {
            let user = new User(req.body);
            user.save();
            if (req.body.equipo) {
                let equipo = await Team.update(
                    { name: req.body.equipo },
                    { $push: { players: user } }
                );
            }
            res.status(200).send(user);
        }
    } catch (error) {
        res.status(500).send({
            "status": "fail"
        });
    }
});

app.post("/equipo", async function (req, res) {
    try {
        let exists = await Team.findOne({ name: req.body.name });
        if (exists !== null) {
            res.status(200).send({
                error: "Equipo ya existe"
            });
        } else {
            let team = new Team(req.body);
            team.save();
            res.status(200).send(team);
        }
    } catch (error) {
        res.status(500).send({
            "status": "fail"
        });
    }
});

app.post("/cupon", async function (req, res) {
    let user = await User.findOne({ rut: req.body.rut });
    if (user.team !== '') {
        let team = await Team.findOne({ name: user.team });
        team.players.push(req.body);
        team.save();
    }
    user.cantidad += req.body.cantidad;
    user.puntaje += req.body.puntaje;
    user.save();

    res.status(200).send({
        success: "Se ha guardado el puntaje"
    });
});

app.get("/tombola", async function (req, res) {
    try {
        let cupones = await Cupon.find({});
        if (cupones.length > 0) {
            res.status(200).send(cupones)
        } else {
            res.status(200).send({
                "error": "No existen cupones"
            });
        }
    } catch (error) {
        res.status(500).send({
            "status": "fail"
        })
    }
});

app.get("/equipo", async function (req, res) {
    try {
        let equipos = await Team.find({});
        if (equipos.length > 0) {
            res.status(200).send(equipos)
        } else {
            res.status(200).send({
                "error": "No existen equipos"
            });
        }
    } catch (error) {
        res.status(500).send({
            "status": "fail"
        })
    }
});

app.get("/usuarios", async function (req, res) {
    try {
        let usuarios = await User.find({});
        if (usuarios.length > 0) {
            res.status(200).send(usuarios)
        } else {
            res.status(200).send({
                "error": "No existen usuarios"
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send({
            "status": "fail"
        })
    }
});


app.get("/rankingJugador", async function (req, res) {
    try {
        let usuarios = await User.aggregate(
            [
                {
                    $group:
                        {
                            _id: { rut: "$rut", nombre: "$name", local: "$local" },
                            totalAmount: { $sum: "$puntaje" },
                            count: { $sum: 1 },
                        }
                },
                { $sort: { totalAmount: -1 } }
            ]
        );
        res.status(200).send(usuarios);
    } catch (error) {
        console.log(error);
    }
});

app.get("/rankingTeam", async function (req, res) {
    try {
        let teams = await Team.aggregate([
            {
                "$unwind": "$players"
            },
            {
                "$group": {
                    "_id": "$name",
                    "total": {
                        "$sum": "$players.puntaje"
                    }
                }
            },
            {
                "$project": {
                    "nombre": "$_id",
                    "total": 1
                }
            },
            { $sort: { total: -1 } }
        ]);
        return res.status(200).send(teams);
    } catch (error) {
        console.log(error);
    }
})